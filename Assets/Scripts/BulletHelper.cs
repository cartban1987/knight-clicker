﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHelper : MonoBehaviour
{

    HeathlHelper _healtHelper;

    public int Damage { get; set; }

    // Start is called before the first frame update
    void Start()
    {

       
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_healtHelper == null)
            _healtHelper = GameObject.FindObjectOfType<HeathlHelper>();
        else
        {
            transform.position = Vector2.MoveTowards(transform.position,
                _healtHelper.transform.position,
                Time.deltaTime * 15);

            if (Vector2.Distance(transform.position,
                _healtHelper.transform.position) < 0.1f)
            {
                _healtHelper.GetHit(Damage);

                Destroy(gameObject);
            }
        }
    }
}
