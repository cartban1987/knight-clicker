﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitHelper : MonoBehaviour
{
    GameHelper _gameHelper;
    PlayerHelper _playerHelper;
    void Start()
    {
        _gameHelper = GameObject.FindObjectOfType<GameHelper>();
        _playerHelper = GameObject.FindObjectOfType<PlayerHelper>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        Debug.Log("OnMouseDown()");
        if (_gameHelper.EndGame)
            return;
        GetComponent<HeathlHelper>().GetHit(_gameHelper.PlayerDamage);

        _playerHelper.RunAttack();
    }
}
